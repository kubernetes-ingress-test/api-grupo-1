package com.api;

public class Response {
    private Integer code;
    private String message;
    private String aplicacion;

    public Response() {
    }

    public Response(Integer code, String message, String aplicacion) {
        this.code = code;
        this.message = message;
        this.aplicacion = aplicacion;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(String aplicacion) {
        this.aplicacion = aplicacion;
    }

        
}
